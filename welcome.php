<?php 
session_start();
include_once 'conexion.php';
if (isset($_SESSION['usuario'])) {
?>
    <!DOCTYPE html>
    <html lang="es">
        <head>
            <meta charset="utf-8">
            <title>Mixfuckedup Sound Lab</title>
            <link rel="stylesheet" type="text/css" href="css/lab.css">
        </head>
        <body>
            <div class="contenedor">
                <h1>Hola Estas adentro <?php echo $_SESSION['usuario'] ?></h1>
                <h2>Aqui debe haber aplicaciones interactivas de audio web</h2>
                <span><h3>Agrega un usuario</h3></span>
                <form method="POST" action="ingresar_usuario.php" name="form">
                        <p> <input type="text" id="user" value="" name="userIn" placeholder="Usuario" required="true"></p>
                        <p><input type="password" id="pass" value="" name="passIn" placeholder="Contrasena" required="true"></p>
                        <p><input type="password" id="repitePass" value="" name="repassIn" placeholder="Repite la Contrasena" required="true"></p>
                        <p><input type="submit" value="Insertar datos" ></p>
                    </form>
                <a href="logout.php">Cierra tu sesion</a>
                <div class="resultado">
                </div>
            </div>
            <script type="text/javascript" src="js/lab.js"></script>
            <script type="text/javascript" src="js/jquery.1.11.1.min.js"></script>
        </body>
    </html>
<?php 
} else {
    echo '<script>location.href = "index.php";</script>'; 
}
?>